# Python cookiecutter template

In order to generate a python application based on a Cookiecutter project template, follow the procedure described below.

* Log on your Developer Cloud Sandbox instance, and type:
  <pre><code>
  sudo yum install miniconda -y
  sudo conda install cookiecutter -y
  </code></pre>

* Type:
  <pre><code>
  export PATH=/opt/anaconda/bin/:$PATH
  cookiecutter https://gitlab.com/ellip/cookiecutter/workflows/cookiecutter-ewf-python.git  
  </code></pre>
  
* Set the 'artifactId', 'summary', 'description', 'version', 'community' and 'groupId' values for your project, then select the python version and finally set the name of the environment to use for running the application. For example:
  <pre><code>
  artifactId [ewf-some-app]:
  summary [This is a summary]:
  description [This is a short description]:
  version [0.1]: 0.1-SNAPSHOT
  community [community]:
  groupId [com.terradue]:
  Select python:
  1 - 2.7
  2 - 3.6
  3 - 3.7
  Choose from 1, 2, 3 [1]:
  environment [env_ewf_some_app]:
  </code></pre>

* After that, you will see a folder with the name of the artifactId (here *ewf-some-app*), where you can start writing your application.

  <pre><code>
  ewf-some-app/
  ├── Jenkinsfile
  ├── pom.xml
  ├── README.md
  └── src
      ├── main
      │   └── app-resources
      │       ├── application.xml
      │       ├── dependencies
      │       │   └── python
      │       ├── node_A
      │       │   └── run
      │       ├── node_B
      │       │   └── run
      │       └── util
      │           └── util.py
      └── test
          └── test_util.py
  </code></pre>

Moreover, the cookiecutter project generation also includes the creation of a Jupyter kernel for new the python environnment. If you have Jupyter installed on your developer cloud sandbox, the new python kernel will be available to use for a notebook.


To install new python packages in your conda environment use the following command:

  <pre><code>
  sudo conda install -y -n env_ewf_some_app <package 1> <package 2>
  </code></pre>

To install your application on the developer cloud sandbox, navigate to the root folder of your project, activate the conda environment and run the maven install command:
  <pre><code>
  cd ewf-some-app
  conda activate env_ewf_some_app
  mvn clean install
  </code></pre>


