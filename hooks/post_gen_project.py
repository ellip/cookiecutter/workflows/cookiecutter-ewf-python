import subprocess

def launch_process(args):
 
    process = subprocess.Popen(args,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT)
   

    stdout = process.communicate()[0]

    if process.returncode != 0 :

        message='Error executing {} ({})'.format(' '.join(args), process.returncode)
    
    else: 
    
        message='Execution completed ({})'.format(process.returncode)

    return message, stdout

steps = ['Updating conda...',
        'Creating the {{cookiecutter.environment}} conda environment with Python {{cookiecutter.python}}...',
        'Configuring a Jupyter kernel for the {{cookiecutter.environment}} conda environment']


commands = ['sudo conda update conda -y',
            'sudo conda create --name {{cookiecutter.environment}} python={{cookiecutter.python}} cioppy lxml -y',
            '/opt/anaconda/envs/{{cookiecutter.environment}}/bin/python -m ipykernel install --name {{cookiecutter.environment}}']


for index, command in enumerate(commands):
    print(steps[index])
    launch_process(command.split(' '))
  
print ('Install Python packages with:\n')
print ('sudo conda install -y -n {{cookiecutter.environment}} <package 1> <package 2>\n')

print ('Activate the conda environment with:\n')
print ('conda activate {{cookiecutter.environment}}')
